package persistance;

import org.mongolink.MongoSession;
import domain.CarRepository;
import domain.Repositories;

public class MongoRepositories extends Repositories {

    public MongoRepositories(MongoSession session) {
        this.session = session;
    }

    @Override
    protected CarRepository carsRepository() {
        return new CarMongoRepository(session);
    }

    private MongoSession session;


}
