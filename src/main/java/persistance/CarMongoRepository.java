package persistance;

/**
 * Created by clark on 11/1/2016.
 */
import org.mongolink.MongoSession;
import domain.Car;
import domain.CarRepository;

public class CarMongoRepository extends MongoRepository<Car> implements CarRepository {
    public CarMongoRepository(MongoSession mongoSession) {
        super(mongoSession);
    }

}