package persistance.mapping;



import org.mongolink.domain.mapper.AggregateMap;
import org.mongolink.domain.mapper.SubclassMap;
import domain.Car;

@SuppressWarnings("UnusedDeclaration")
public class CarMapping extends AggregateMap<Car> {

    @Override
    public void map() {
        id().onProperty(element().getId()).natural();
        property().onField("name");
        property().onProperty(element().getCreationDate());
        /*subclass(new SubclassMap<Car>() {

            @Override
            public void map() {

            }
        });*/
    }
}
