import static spark.Spark.*;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.google.common.collect.Lists;
import com.mongodb.ServerAddress;
import domain.Car;
import domain.Repositories;

import org.mongolink.MongoSession;
import org.mongolink.MongoSessionManager;
import org.mongolink.Settings;
import org.mongolink.UpdateStrategies;
import org.mongolink.domain.mapper.ContextBuilder;
import persistance.MongoRepositories;

import java.io.IOException;
import java.io.StringWriter;


public class HelloWorld {
    public static void main(String[] args) {

        //create dummy objects and arrays

        get("/", (req, res) -> "Hello, World!");
        post("/", (req, res) -> {

            //final MongoSession session = MongoConfiguration.createSession();
            ContextBuilder builder = new ContextBuilder("persistance.mapping");
            Settings settings = Settings.defaultInstance()
                    .withDefaultUpdateStrategy(UpdateStrategies.DIFF)
                    .withDbName("cmu_sv_app")
                    .withAddresses(Lists.newArrayList(new ServerAddress("ds035826.mlab.com", 35826)))
                    .withAuthentication("app_user", "password");
            MongoSessionManager mongoSessionManager = MongoSessionManager.create(builder, settings);
            MongoSession session = mongoSessionManager.createSession();
            session.start();
            Repositories.initialise(new MongoRepositories(session));

            try{
                ObjectMapper mapper = new ObjectMapper();
                Car car = mapper.readValue(req.body(), Car.class);

                if (!car.isValid()) {
                    res.status(400);
                    return "";
                }


                Repositories.cars().add(car);
                session.stop();
                res.status(200);
                res.type("application/json");
                return dataToJson(car);

            }catch (JsonParseException e){
                session.stop();
                res.status(400);
                return "";
            }

        });

    }

    public static String dataToJson(Object data) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            StringWriter sw = new StringWriter();
            mapper.writeValue(sw, data);
            return sw.toString();
        } catch (IOException e){
            throw new RuntimeException("IOException from a StringWriter?");
        }
    }
}