package domain;

import org.joda.time.DateTime;

import java.util.UUID;
/**
 * Created by clark on 11/1/2016.
 */
interface Validable {
    boolean isValid();
}

public class Car implements Validable {

    @SuppressWarnings("UnusedDeclaration")
    protected Car() {
        // for mongolink
    }

    public Car(String name) {
        this.name = name;
        this.id = UUID.randomUUID();
    }

    public UUID getId() { return id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    private UUID id;
    private String name;
    private DateTime creationDate = new DateTime();

    public boolean isValid(){
        //Could set up any additional validation rule
        return true;
    }

}
