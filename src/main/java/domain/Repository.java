package domain;

import java.util.List;
/**
 * Created by clark on 11/1/2016.
 */
public interface Repository<T> {

    T get(Object id);

    void delete(T entité);

    void add(T entité);

    List<T> all();
}
