package domain;

/**
 * Created by clark on 11/1/2016.
 */
public abstract class Repositories {

    public static void initialise(Repositories instance) {
        Repositories.instance = instance;
    }

    public static CarRepository cars() {
        return instance.carsRepository();
    }

    protected abstract CarRepository carsRepository();
    //protected abstract DriverRepository driversRepository();

    private static Repositories instance;
}
